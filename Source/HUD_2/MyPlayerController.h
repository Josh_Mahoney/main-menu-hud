// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class HUD_2_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public: 

	//Ref to UMG Asset
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> wMainMenu;

	//Variable that holds widget after creation
	UUserWidget* MyMainMenu;

	//Override BeginPlay()
	virtual void BeginPlay() override;
	
	
};
	
	
