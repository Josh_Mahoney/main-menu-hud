// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPlayerController.h"
#include "Blueprint/UserWidget.h"

void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (wMainMenu) // Checks for the blueprint
	{
		//Creates widget and stores location
		MyMainMenu = CreateWidget<UUserWidget>(this, wMainMenu);

		if (MyMainMenu)
		{
			//lets it be added to viewport
			MyMainMenu->AddToViewport();
		}

		//Shows the mouse cursor
		bShowMouseCursor = true;
	}
}




